package com.image_activities.animation_startup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val ttb = AnimationUtils.loadAnimation(this,R.anim.ttb)
        val stb = AnimationUtils.loadAnimation(this,R.anim.stb)

        val btt = AnimationUtils.loadAnimation(this,R.anim.btt)
        val btt2 = AnimationUtils.loadAnimation(this,R.anim.btt2)
        val btt3 = AnimationUtils.loadAnimation(this,R.anim.btt3)
        val btn_anim = AnimationUtils.loadAnimation(this,R.anim.btn_anim)

        val headertitle = findViewById<TextView>(R.id.headertitle)as TextView
        val subtitle = findViewById<TextView>(R.id.subtitle)as TextView
        val ic_cards = findViewById<ImageView>(R.id.ic_cards)as ImageView

        val resultOne = findViewById<LinearLayout>(R.id.resultOne)as LinearLayout
        val resultTwo =findViewById<LinearLayout>(R.id.resultTwo)as LinearLayout
        val resultThree = findViewById<LinearLayout>(R.id.resultThree)as LinearLayout

        val btn_next_course = findViewById<Button>(R.id.btn_next_course)as Button

        headertitle.startAnimation(ttb)
        subtitle.startAnimation(ttb)
        ic_cards.startAnimation(stb)

        resultOne.startAnimation(btt)
        resultTwo.startAnimation(btt2)
        resultThree.startAnimation(btt3)

        btn_next_course.startAnimation(btn_anim)

    }
}